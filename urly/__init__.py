from flask import Flask
from .database import init_db

app = Flask(__name__)
conn, c = init_db()

import urly.core
