from flask import render_template, request, redirect, abort
from config import SHORTY_LENGTH, TIMER, VALIDATION
from .validator import URL_REGEX
from . import app, conn, c
from sqlite3 import IntegrityError
from json import dumps
from random import choice
from string import ascii_letters, digits
from time import time


@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'GET':
        return render_template('index.html')
    domain = request.url_root
    failed = lambda x: {'mail': x}
    success = lambda x: {'url': domain + x}    
    data = request.form.get('data')
    if not data or len(data.strip()) < 4:
        return dumps(failed('Something goes wrong'))
    link = data.strip()
    if not link.lower().startswith(('http://', 'https://')): 
        link = 'http://' + link
    if not tester(link):
        return dumps(failed('Invalid URL'))
    shorty = checker(link)
    if shorty:
        return dumps(success(shorty[0]))
    shorty = generator()
    if not shorty:
        return dumps(failed('Too much time to generate shorty..'))
    try:
        c.execute('INSERT INTO urly VALUES (NULL, ?, ?)', (link, shorty))
        conn.commit()
    except IntegrityError:
        conn.rollback()
        return dumps(failed('Something goes wrong'))
    return dumps(success(shorty))


@app.route('/<path>')
def kicker(path):
    if len(path) != SHORTY_LENGTH or not path.isalnum():
        abort(404)
    visa = checker(path, False)
    return redirect(visa[0]) if visa else abort(404)


def generator():
    go = lambda x: ''.join(choice(ascii_letters + digits) for i in range(x))
    start = time()
    while time() - start <= TIMER:
        key = go(SHORTY_LENGTH)
        c.execute('SELECT url FROM urly WHERE shorty=?', (key, ))
        if c.fetchone():
            continue
        return key
    return False


def checker(link, status=True):
    x = ('shorty', 'url') if status else ('url', 'shorty')
    c.execute('SELECT {} FROM urly WHERE {}=?'.format(*x), (link, ))
    return c.fetchone() or False


def tester(link):
    if not VALIDATION:
        return True
    host = request.host
    pattern = URL_REGEX.match(link)
    return False if not pattern or host in link else True


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404
