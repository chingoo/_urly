$(document).ready(function(){
    var status = true;
    $("#url").focus();
    $("#submit").click(function (){
        status = true;
        deleter();
        if($("#url").val().trim().length < 4){
            $("#shorty").addClass("has-error");
            $("#url").focus();
            $("#url").attr("data-content", "Too shorty");
            $("#url").popover("show");
        } else {
            var data = {"data": $("#url").val().trim()};
            $.post("/", data).done(function(data){
                var response = $.parseJSON(data);
                if(response.url){
                    $("#shorty").addClass("has-success");
                    $("#url").popover("destroy");
                    $("#url").val(response.url);
                    $("#url").focus();
                    $("#url").select();
                } else {
                    $("#shorty").addClass("has-error");
                    $("#url").focus();
                    $("#url").attr("data-content", response.mail);
                    $("#url").popover("show");
                };
            });
        };
    });
    $("#url").keydown(function(e){
        if(e.which == 13){
            $("#submit").click();
        } else {
            deleter();
        };
    });
    $("#url").click(
        function(){
            status = true;
            deleter();
        });
    $("#url").blur(
        function(){
            status = false;
            deleter();         
        });
    $("#url").hover(
        function(){
            if(!status){
                $("#url").focus();
            };
        },
        function(){
            if(!status){
                $("#url").blur();
            };
        }
    );
});

function deleter(){
    $("#shorty").removeClass("has-error");
    $("#shorty").removeClass("has-success");
    $("#url").popover("destroy"); 
};
