from sqlite3 import connect, OperationalError
from config import IN_MEMORY
from os.path import isfile

db = ':memory:' if IN_MEMORY else 'urly.db'


def init_db():
    conn = connect(db, check_same_thread=False)
    c = conn.cursor()
    if isfile('urly.db') and c.execute('PRAGMA table_info(urly)').fetchone():
        return conn, c
    try:
        c.execute("""CREATE TABLE urly (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            url TEXT NOT NULL UNIQUE,
            shorty TEXT NOT NULL UNIQUE)""")
    except OperationalError:
        print('\033[31m' + 'Something goes wrong!' + '\033[0m')
    return conn, c
