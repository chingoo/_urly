## _urly

** Minimalistic url shortener **

### Description

Written on Python 3 and uses SQLite database.

### Install

```
git clone https://bitbucket.org/chingoo/_urly.git
cd _urly
pyvenv venv
source venv/bin/activate
pip install -r requirements.txt
deactivate
```

### Config

In `config.py` you can find some params to change.

### Use

Activate virtual environment with `source venv/bin/activate`. Then run web server:

    gunicorn run:urly --bind '0.0.0.0'
    
_urly will welcome you on `<host>:8000`.

### Screen

![_urly.png](https://bitbucket.org/repo/oebn9A/images/490197756-_urly.png)