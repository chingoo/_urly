SHORTY_LENGTH = 4  # Length of shorty path, http://<host>/<shorty>
TIMER = 2  # How much seconds you are ready to wait?
IN_MEMORY = True  # False to save database
VALIDATION = True  # False to bypass validator
